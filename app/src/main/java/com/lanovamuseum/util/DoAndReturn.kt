package com.lanovamuseum.util

object DoAndReturn {
    inline fun <R> doAndReturn(value: R, action: () -> Unit): R {
        action()
        return value
    }
}