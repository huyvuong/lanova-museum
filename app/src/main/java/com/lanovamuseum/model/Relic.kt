package com.lanovamuseum.model

data class Relic(
        val name: String,
        val description: String,
        val type: String,
        val costInChaosGems: Int,
        val costInGold: Int,
        val pool: Int?
)