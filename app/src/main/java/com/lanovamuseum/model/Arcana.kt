package com.lanovamuseum.model

data class Arcana(
        val name: String,
        val description: String,
        val element: String,
        val type: String,
        val damage: List<Int>,
        val knockback: List<Int>,
        val cooldown: Int,
        val duration: Int?,
        val costInChaosGems: Int,
        val costInGold: Int,
        val pool: Int?
)