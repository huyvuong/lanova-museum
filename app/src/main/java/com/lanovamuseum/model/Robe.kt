package com.lanovamuseum.model

data class Robe(
        val name: String,
        val effects: List<String>
)