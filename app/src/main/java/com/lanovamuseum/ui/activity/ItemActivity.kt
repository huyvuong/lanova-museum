package com.lanovamuseum.ui.activity

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.lanovamuseum.R
import com.lanovamuseum.model.Arcana
import com.lanovamuseum.model.Relic
import com.lanovamuseum.model.Robe
import com.lanovamuseum.repository.data.ArcanaRepository
import com.lanovamuseum.repository.data.RelicRepository
import com.lanovamuseum.repository.data.RobeRepository
import com.lanovamuseum.repository.data.database.LanovaMuseumDatabase
import com.lanovamuseum.ui.adapter.ArcanaAdapter
import com.lanovamuseum.ui.adapter.RelicAdapter
import com.lanovamuseum.ui.adapter.RobeAdapter
import com.lanovamuseum.util.DoAndReturn.doAndReturn
import kotlinx.android.synthetic.main.activity_item.itemsRecyclerView
import kotlinx.android.synthetic.main.activity_item.navigation

class ItemActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    private lateinit var searchView: SearchView
    private val relicRepository: RelicRepository by lazy {
        RelicRepository(LanovaMuseumDatabase.getInstance(this).relicDao())
    }
    private val arcanaRepository: ArcanaRepository by lazy {
        ArcanaRepository(LanovaMuseumDatabase.getInstance(this).arcanaDao())
    }
    private val robeRepository: RobeRepository by lazy {
        RobeRepository(LanovaMuseumDatabase.getInstance(this).robeDao())
    }
    private val relicObserver = Observer { relics: List<Relic> ->
        itemsRecyclerView.adapter = RelicAdapter(relics)
        itemsRecyclerView.invalidate()
    }
    private val arcanaObserver = Observer { arcana: List<Arcana> ->
        itemsRecyclerView.adapter = ArcanaAdapter(arcana)
        itemsRecyclerView.invalidate()
    }
    private val robeObserver = Observer { robes: List<Robe> ->
        itemsRecyclerView.adapter = RobeAdapter(robes)
        itemsRecyclerView.invalidate()
    }
    private val onQueryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(queryText: String?): Boolean =
                doAndReturn(true) { showItemsForQueryText(queryText) }

        override fun onQueryTextChange(queryText: String?): Boolean =
                doAndReturn(true) { showItemsForQueryText(queryText) }

        private fun showItemsForQueryText(searchQuery: String?) {
            if (searchQuery == null || searchQuery.isEmpty()) {
                listAllItems(getSelectedNavigationItemId())
            } else {
                listMatchingItems(searchQuery)
            }
        }
    }
    private val onActionExpandListener = object : MenuItem.OnActionExpandListener {
        override fun onMenuItemActionExpand(menuItem: MenuItem?): Boolean = true

        override fun onMenuItemActionCollapse(menuItem: MenuItem?): Boolean =
                doAndReturn(true) { listAllItems(getSelectedNavigationItemId()) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item)
        handleIntent(intent)

        navigation.setOnNavigationItemSelectedListener(this)
        itemsRecyclerView.layoutManager = LinearLayoutManager(this)
        showRelics()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)

        val searchMenuItem =
                menu.findItem(R.id.search)
                        .apply { setOnActionExpandListener(onActionExpandListener) }
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = searchMenuItem.actionView as SearchView
        searchView.apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setOnQueryTextListener(onQueryTextListener)
            queryHint = when (getSelectedNavigationItemId()) {
                R.id.navigation_relics -> getString(R.string.search_hint_relics)
                R.id.navigation_arcana -> getString(R.string.search_hint_arcana)
                R.id.navigation_robes -> getString(R.string.search_hint_robes)
                else -> getString(R.string.search_hint_default)
            }
        }

        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.navigation_relics -> doAndReturn(true, ::showRelics)
                R.id.navigation_arcana -> doAndReturn(true, ::showArcana)
                R.id.navigation_robes -> doAndReturn(true, ::showRobes)
                else -> false
            }

    private fun getSelectedNavigationItemId(): Int =
            navigation.menu.findItem(navigation.selectedItemId).itemId

    private fun listAllItems(@IdRes itemId: Int) {
        when (itemId) {
            R.id.navigation_relics -> relicRepository.getAll().observe(this, relicObserver)
            R.id.navigation_arcana -> arcanaRepository.getAll().observe(this, arcanaObserver)
            R.id.navigation_robes -> robeRepository.getAll().observe(this, robeObserver)
        }
    }

    private fun listMatchingItems(searchQuery: String) {
        when (getSelectedNavigationItemId()) {
            R.id.navigation_relics ->
                relicRepository.search(searchQuery).observe(this, relicObserver)
            R.id.navigation_arcana ->
                arcanaRepository.search(searchQuery).observe(this, arcanaObserver)
            R.id.navigation_robes ->
                robeRepository.search(searchQuery).observe(this, robeObserver)
        }
    }

    private fun showRelics() {
        showItems(getString(R.string.title_relics), R.id.navigation_relics, getString(R.string.search_hint_relics))
    }

    private fun showArcana() {
        showItems(getString(R.string.title_arcana), R.id.navigation_arcana, getString(R.string.search_hint_arcana))
    }

    private fun showRobes() {
        showItems(getString(R.string.title_robes), R.id.navigation_robes, getString(R.string.search_hint_robes))
    }

    private fun showItems(actionBarTitle: String, @IdRes itemId: Int, queryHint: String) {
        supportActionBar?.title = actionBarTitle
        listAllItems(itemId)

        if (::searchView.isInitialized) {
            searchView.queryHint = queryHint
        }
    }

    private fun handleIntent(intent: Intent?) {
        if (Intent.ACTION_SEARCH == intent?.action) {
            listMatchingItems(SearchManager.QUERY)
        }
    }
}
