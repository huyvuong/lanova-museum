package com.lanovamuseum.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.lanovamuseum.R
import com.lanovamuseum.model.Arcana
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_arcana.costInChaosGemsView
import kotlinx.android.synthetic.main.item_arcana.costInGoldView
import kotlinx.android.synthetic.main.item_arcana.descriptionView
import kotlinx.android.synthetic.main.item_arcana.elementView
import kotlinx.android.synthetic.main.item_arcana.iconView
import kotlinx.android.synthetic.main.item_arcana.nameView
import kotlinx.android.synthetic.main.item_arcana.poolView
import kotlinx.android.synthetic.main.item_arcana.typeView

class ArcanaAdapter(private val arcanas: List<Arcana>) :
        RecyclerView.Adapter<ArcanaAdapter.ViewHolder>() {

    override fun getItemCount(): Int = arcanas.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArcanaAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_arcana, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ArcanaAdapter.ViewHolder, position: Int) {
        holder.bindArcana(arcanas[position])
    }

    class ViewHolder(override val containerView: View) :
            RecyclerView.ViewHolder(containerView),
            LayoutContainer {
        fun bindArcana(arcana: Arcana) {
            with(arcana) {
                val elementColorId = when (element) {
                    "Air" -> R.color.textColorElementAir
                    "Fire" -> R.color.textColorElementFire
                    "Water" -> R.color.textColorElementWater
                    "Lightning" -> R.color.textColorElementLightning
                    "Earth" -> R.color.textColorElementEarth
                    "Chaos" -> R.color.textColorElementChaos
                    else -> R.color.iconColorPlaceholder
                }
                val elementColor = ContextCompat.getColor(containerView.context, elementColorId)

                nameView.text = name
                descriptionView.text = description
                elementView.text = element
                elementView.setTextColor(elementColor)
                typeView.text = type
                costInChaosGemsView.text = "$costInChaosGems CG"
                costInGoldView.text = "$costInGold G"
                poolView.text = if (pool != null) " (Pool $pool)" else ""
                iconView.drawable.mutate().setTint(elementColor)
                iconView.contentDescription = name
            }
        }
    }
}
