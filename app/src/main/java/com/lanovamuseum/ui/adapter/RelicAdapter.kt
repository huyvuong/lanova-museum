package com.lanovamuseum.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.lanovamuseum.R
import com.lanovamuseum.model.Relic
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_relic.costInChaosGemsView
import kotlinx.android.synthetic.main.item_relic.costInGoldView
import kotlinx.android.synthetic.main.item_relic.descriptionView
import kotlinx.android.synthetic.main.item_relic.iconView
import kotlinx.android.synthetic.main.item_relic.nameView
import kotlinx.android.synthetic.main.item_relic.poolView
import kotlinx.android.synthetic.main.item_relic.typeView

class RelicAdapter(private val relics: List<Relic>) :
        RecyclerView.Adapter<RelicAdapter.ViewHolder>() {

    override fun getItemCount(): Int = relics.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RelicAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_relic, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RelicAdapter.ViewHolder, position: Int) {
        holder.bindRelic(relics[position])
    }

    class ViewHolder(override val containerView: View) :
            RecyclerView.ViewHolder(containerView),
            LayoutContainer {
        fun bindRelic(relic: Relic) {
            with(relic) {
                DrawableCompat.setTint(
                        iconView.drawable,
                        ContextCompat.getColor(containerView.context, R.color.iconColorPlaceholder))
                iconView.contentDescription = name
                nameView.text = name
                descriptionView.text = description
                typeView.text = type
                costInChaosGemsView.text = "$costInChaosGems CG"
                costInGoldView.text = "$costInGold G"
                poolView.text = if (pool != null) " (Pool $pool)" else ""
            }
        }
    }
}
