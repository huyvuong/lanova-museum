package com.lanovamuseum.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.lanovamuseum.R
import com.lanovamuseum.model.Robe
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_robe.*

class RobeAdapter(private val robes: List<Robe>) :
        RecyclerView.Adapter<RobeAdapter.ViewHolder>() {

    override fun getItemCount(): Int = robes.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RobeAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_robe, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RobeAdapter.ViewHolder, position: Int) {
        holder.bindRelic(robes[position])
    }

    class ViewHolder(override val containerView: View) :
            RecyclerView.ViewHolder(containerView),
            LayoutContainer {
        fun bindRelic(robe: Robe) {
            with(robe) {
                val robeColorId = when (name) {
                    "Hope" -> R.color.iconColorRobeHope
                    "Patience" -> R.color.iconColorRobePatience
                    "Vigor" -> R.color.iconColorRobeVigor
                    "Grit" -> R.color.iconColorRobeGrit
                    "Tempo" -> R.color.iconColorRobeTempo
                    "Pace" -> R.color.iconColorRobePace
                    "Shift" -> R.color.iconColorRobeShift
                    "Awe" -> R.color.iconColorRobeAwe
                    "Rule" -> R.color.iconColorRobeRule
                    "Venture" -> R.color.iconColorRobeVenture
                    "Pride" -> R.color.iconColorRobePride
                    else -> R.color.iconColorPlaceholder
                }
                val robeColor = ContextCompat.getColor(containerView.context, robeColorId)

                nameView.text = name
                effectsView.text = effects.asSequence().map { " • $it" }.joinToString("\n")
                iconView.drawable.mutate().setTint(robeColor)
                iconView.contentDescription = name
            }
        }
    }
}
