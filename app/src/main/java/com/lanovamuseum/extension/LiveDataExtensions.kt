package com.lanovamuseum.extension

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations

object LiveDataExtensions {
    fun <X, Y> LiveData<X>.map(mapFunction: (X) -> Y): LiveData<Y> =
            Transformations.map(this) { mapFunction(it) }
}
