package com.lanovamuseum.repository.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.lanovamuseum.repository.data.entity.Robe

@Dao
interface RobeDao {
    @Query("SELECT * FROM robes;")
    fun getAll(): LiveData<List<Robe>>

    @Query("SELECT * FROM robes WHERE name LIKE '%' || :searchQuery || '%' OR effects LIKE '%' || :searchQuery || '%' COLLATE NOCASE;")
    fun search(searchQuery: String): LiveData<List<Robe>>

    @Insert
    fun insertAll(relics: List<Robe>)
}