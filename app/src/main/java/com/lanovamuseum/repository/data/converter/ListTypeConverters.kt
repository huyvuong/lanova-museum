package com.lanovamuseum.repository.data.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ListTypeConverters {
    private val gson: Gson by lazy { Gson() }

    @TypeConverter
    fun jsonToListOfString(json: String): List<String> =
            gson.fromJson(json, object : TypeToken<List<String>>() {}.type)

    @TypeConverter
    fun listOfStringToJson(strings: List<String>): String = gson.toJson(strings)

    @TypeConverter
    fun jsonToListOfInt(json: String): List<Int> =
            gson.fromJson(json, object : TypeToken<List<Int>>() {}.type)

    @TypeConverter
    fun listOfIntToJson(ints: List<Int>): String = gson.toJson(ints)
}