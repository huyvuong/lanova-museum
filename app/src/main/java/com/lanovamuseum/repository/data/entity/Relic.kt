package com.lanovamuseum.repository.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "relics")
data class Relic(
        @PrimaryKey(autoGenerate = true) var id: Long?,
        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "description") var description: String,
        @ColumnInfo(name = "type") var type: String,
        @ColumnInfo(name = "cost_in_chaos_gems") var costInChaosGems: Int,
        @ColumnInfo(name = "cost_in_gold") var costInGold: Int,
        @ColumnInfo(name = "pool") var pool: Int?
) {
    constructor() : this(null, "", "", "", 0, 0, null)

    constructor(
            name: String,
            description: String,
            type: String,
            costInChaosGems: Int,
            costInGold: Int,
            pool: Int?
    ) : this(null, name, description, type, costInChaosGems, costInGold, pool)

    companion object {
        val ALL_RELICS =
                listOf(
                        Relic(
                                "Abhorrent Cologne",
                                "Enemies have less health, but enemy damage is increased!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Absorption Coil",
                                "A percentage of damage taken charges your signature arcana!",
                                "Misc",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Agni's Sparkler",
                                "Increases ferocity of your burn status effects!",
                                "Offense",
                                23,
                                175,
                                4
                        ),
                        Relic(
                                "Albert's Formula",
                                "Receive signature charge when healed!",
                                "Misc",
                                17,
                                125,
                                3
                        ),
                        Relic(
                                "Alchemist's Stone",
                                "All Chaos Gems gained during the Chaos Trials are transmuted into gold!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Amulet of Sundering",
                                "Increases all damage dealt!",
                                "Offense",
                                10,
                                125,
                                1
                        ),
                        Relic(
                                "Analytical Monocle",
                                "Increases critical hit chance!",
                                "Offense",
                                10,
                                125,
                                1
                        ),
                        Relic(
                                "Anchor of Burden",
                                "Damage and stuns are increased but movement speed is reduced for each cursed relic owned!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Ancient Fountain Pen",
                                "Your charged signature arcana deals more damage every time you use it!",
                                "Offense",
                                22,
                                150,
                                4
                        ),
                        Relic(
                                "Antiquated Tabi",
                                "Adds a small chance to evade attacks!",
                                "Defense",
                                10,
                                125,
                                1
                        ),
                        Relic(
                                "Arcana Rewards Card",
                                "All gold costs for arcana are discounted!",
                                "Misc",
                                12,
                                100,
                                2
                        ),
                        Relic(
                                "Armor of Greed",
                                "Grants a significant boost in armor! Every hit taken reduces gold count.",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Armor of Resolve",
                                "Increases armor while at low health!",
                                "Defense",
                                27,
                                175,
                                5
                        ),
                        Relic(
                                "Assassin's Blade",
                                "Increases critical hit damage!",
                                "Offense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Auditor's Talisman",
                                "Increases critical hit chance at low health!",
                                "Offense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Auger of Poetry",
                                "Increases resistance to earth based attacks!",
                                "Defense",
                                15,
                                175,
                                2
                        ),
                        Relic(
                                "Autograph Pad",
                                "Damage and movement speed is increased while signature arcana is charged!",
                                "Offense",
                                25,
                                125,
                                5
                        ),
                        Relic(
                                "Battery of Taranis",
                                "Increases lightning damage!",
                                "Offense",
                                14,
                                150,
                                2
                        ),
                        Relic(
                                "Berserker's Axe",
                                "Increases damage dealt for a short while after taking damage!",
                                "Offense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Berserker's Helm",
                                "Damage increased while at low health!",
                                "Offense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Bladed Buckler",
                                "Destroying an enemy projectile grants signature charge energy!",
                                "Misc",
                                27,
                                175,
                                5
                        ),
                        Relic(
                                "Blasting Sprite Aura",
                                "Summons a sprite that slows enemies! Only one sprite can be active at any time.",
                                "Misc",
                                10,
                                125,
                                1
                        ),
                        Relic(
                                "Boots of Frenzy",
                                "Defeating enemies grants a chance to temporarily add unlimited charges to your movement arcana!",
                                "Misc",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Bracers of the Beast",
                                "Pressing forward into an attack immediately before impact allows you to guard the attack!",
                                "Defense",
                                22,
                                150,
                                4
                        ),
                        Relic(
                                "Broken Plague Flask",
                                "Enemy max health is lowered, but your health is lowered as well!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "C-99 Piggy Bank",
                                "Gets heavier as you progress through the trials. Drop from inventory to open.",
                                "Misc",
                                18,
                                0,
                                3
                        ),
                        Relic(
                                "Calcifying Bonemail",
                                "Increases armor for a short duration after taking damage!",
                                "Defense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Calvin's Sandy Shoes",
                                "Adds a chance to evade while at low health!",
                                "Defense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Captain's Ring",
                                "Equipping arcana of differing elements increases damage!",
                                "Offense",
                                18,
                                150,
                                3
                        ),
                        Relic(
                                "Cartographer's Quill",
                                "Increases damage based on how much of the map you have revealed! Damage increase lasts for the entire trial!",
                                "Offense",
                                27,
                                175,
                                5
                        ),
                        Relic(
                                "Chaos Scanner",
                                "Reveals health bars for all enemies in the Chaos Trials!",
                                "Misc",
                                24,
                                100,
                                5
                        ),
                        Relic(
                                "Charming Teddy Bear",
                                "Adds a chance to charm enemies when taking damage!",
                                "Defense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Cobalt Firestone",
                                "Summons an aura that damages nearby enemies!",
                                "Offense",
                                27,
                                175,
                                5
                        ),
                        Relic(
                                "Combo Gloves",
                                "Basic arcana have an extra combo!",
                                "Misc",
                                10,
                                150,
                                1
                        ),
                        Relic(
                                "Conqueror's Belt",
                                "All Rogues and Archers have their spells limited! Only one Conqueror item can be in effect at a time.",
                                "Misc",
                                29,
                                0,
                                5
                        ),
                        Relic(
                                "Conqueror's Cloak",
                                "All Mages and Summoners have their spells limited! Only one Conqueror item can be in effect at a time.",
                                "Misc",
                                29,
                                0,
                                5
                        ),
                        Relic(
                                "Conqueror's Helmet",
                                "All Knights and Lancers have their spells limited! Only one Conqueror item can be in effect at a time.",
                                "Misc",
                                29,
                                0,
                                5
                        ),
                        Relic(
                                "Covert Ops Mask",
                                "After evading an attack, all of your attacks will be critical hits for a short duration!",
                                "Offense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Crimson Clover",
                                "Critical hit chance is increased for you and all enemies!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Critical Placebos",
                                "Makes you believe all your attacks result in critical hits!",
                                "Doctor",
                                0,
                                200,
                                null
                        ),
                        Relic(
                                "Cushioned Flip-flops",
                                "Damage taken is expended from your signature meter while you have sufficient charge to shield the attack!",
                                "Defense",
                                28,
                                200,
                                5
                        ),
                        Relic(
                                "Dagger of Midas",
                                "Increases damage based on how much gold you currently have!",
                                "Offense",
                                25,
                                125,
                                5
                        ),
                        Relic(
                                "Dark Katana",
                                "Designed by Artisan Mathosse Feadur, this relic triples the critical hit chance for all melee arcana!",
                                "Offense",
                                15,
                                175,
                                2
                        ),
                        Relic(
                                "Dated Sunglasses",
                                "Enemy projectiles move slower, giving you more time to react!",
                                "Misc",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Deafening Cymbals",
                                "Destroys all projectiles in the area when hit by a projectile!",
                                "Defense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Demi's Teapot",
                                "Increases the duration of buff arcana!",
                                "Defense",
                                27,
                                175,
                                5
                        ),
                        Relic(
                                "Destructive Abacus",
                                "Defeating foes lowers active cooldowns!",
                                "Misc",
                                20,
                                200,
                                3
                        ),
                        Relic(
                                "Devi's Bug Spray",
                                "Increases potency of your poison status effects!",
                                "Offense",
                                23,
                                175,
                                4
                        ),
                        Relic(
                                "Dice of the Nemesis",
                                "Adds a small chance of dealing massive damage with your critical hits!",
                                "Offense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Double Edged Cestus",
                                "Increase basic arcana damage! All other damage decreased.",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Double Toil",
                                "Reduces cooldowns by half but receive double damage!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Double Trouble",
                                "Deal double damage but receive double damage!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Ebon Wolf's Cloak",
                                "Increases fire and lightning arcana damage, but receive more damage from water and earth attacks!",
                                "Offense",
                                19,
                                175,
                                3
                        ),
                        Relic(
                                "Elemental Pointes",
                                "Using a dash arcana immediately after another arcana lowers cooldowns for all arcana of the same element as your dash!",
                                "Misc",
                                22,
                                150,
                                4
                        ),
                        Relic(
                                "Elven Ears",
                                "Remaining motionless for a short time causes you to disappear from sight!",
                                "Defense",
                                27,
                                175,
                                5
                        ),
                        Relic(
                                "Equestricap",
                                "Designed by Artisan Tacobowl8, this relic allows you to look quite dashing as you gallop to max speed with ease!",
                                "Defense",
                                14,
                                150,
                                2
                        ),
                        Relic(
                                "Euphie's Shawl",
                                "Increases armor!",
                                "Defense",
                                10,
                                125,
                                1
                        ),
                        Relic(
                                "Evening Gloves",
                                "Basic arcana chains are completed automatically!",
                                "Misc",
                                16,
                                100,
                                3
                        ),
                        Relic(
                                "Flak Gauntlet",
                                "Allows you to destroy enemy projectiles with melee basic arcana!",
                                "Misc",
                                14,
                                150,
                                2
                        ),
                        Relic(
                                "Flashy Boots",
                                "Allows you to triple dash but lowers movement speed!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Fortune Cookie",
                                "Randomly empowers an arcana at the beginning of each stage!",
                                "Offense",
                                27,
                                175,
                                5
                        ),
                        Relic(
                                "Freezing Sprite Naya",
                                "Summons a sprite that freezes enemies! Only one sprite can be active at any time.",
                                "Misc",
                                0,
                                125,
                                1
                        ),
                        Relic(
                                "Friendship Bracelet",
                                "Increase damage and movement speed when you're with a true friend!",
                                "Offense",
                                0,
                                0,
                                1
                        ),
                        Relic(
                                "Fuzzy Handwarmers",
                                "Prevents freeze status effect!",
                                "Defense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Gaia's Shovel",
                                "Increases earth damage!",
                                "Offense",
                                14,
                                150,
                                2
                        ),
                        Relic(
                                "Giant's Heart",
                                "Increases max health!",
                                "Defense",
                                10,
                                125,
                                1
                        ),
                        Relic(
                                "Glass Cannon",
                                "Damage increased but max health is reduced!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Glove of Midas",
                                "Increase gold gain for all wizards! Multiple gloves do not stack.",
                                "Misc",
                                10,
                                125,
                                1
                        ),
                        Relic(
                                "Golden Armor of Envy",
                                "Increases armor but store prices are increased!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Golden Saber of Envy",
                                "Increases damage but store prices are increased!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Greased Boots",
                                "Lowers the cooldown of your dash arcana and adds an extra use!",
                                "Defense",
                                10,
                                150,
                                1
                        ),
                        Relic(
                                "Grimoire of Ruin",
                                "Increases the damage of summoned agents!",
                                "Misc",
                                23,
                                175,
                                4
                        ),
                        Relic(
                                "Gummy Vitamins",
                                "Prevents poison status effect!",
                                "Defense",
                                26,
                                5,
                                5
                        ),
                        Relic(
                                "Health Care Card",
                                "Receive a discount whenever you purchase potions from the merchant!",
                                "Doctor",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Heavy Travel Jacket",
                                "Increases armor for each relic held!",
                                "Defense",
                                18,
                                150,
                                3
                        ),
                        Relic(
                                "Horned Halo",
                                "Revive with low health when defeated but consumes half of current health when picked up! This relic is destroyed on use.",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Hummingbird Feather",
                                "Briefly hover over pits!",
                                "Defense",
                                22,
                                150,
                                4
                        ),
                        Relic(
                                "Hunter's Stiletto",
                                "Increase damage against foes that are inflicted with a status ailment.",
                                "Offense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Hyperbolic Train",
                                "Signature charges faster at low health!",
                                "Misc",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Idealist's Mirror",
                                "All arcana are empowered when at full health!",
                                "Offense",
                                18,
                                150,
                                3
                        ),
                        Relic(
                                "Ifrit's Matchstick",
                                "Increases fire damage!",
                                "Offense",
                                14,
                                150,
                                2
                        ),
                        Relic(
                                "Igniting Sprite Vesa",
                                "Summons a sprite that burns enemies! Only one sprite can be active at any time.",
                                "Misc",
                                18,
                                150,
                                3
                        ),
                        Relic(
                                "Indra's Copper Pipe",
                                "Increases duration of your shock status effects!",
                                "Offense",
                                23,
                                175,
                                4
                        ),
                        Relic(
                                "Infinity Marble",
                                "Signature charge stays active until used!",
                                "Misc",
                                14,
                                150,
                                2
                        ),
                        Relic(
                                "Insignia of Legend",
                                "A crest presented only to those who have cleared the Chaos Trials!",
                                "Misc",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Jewelry Box",
                                "Picking up chaos gems heals for a small amount. Excludes gems collected from Council members.",
                                "Defense",
                                20,
                                200,
                                3
                        ),
                        Relic(
                                "Journal of Midas",
                                "Gain gold when defeating consecutive enemies without taking damage!",
                                "Misc",
                                21,
                                125,
                                4
                        ),
                        Relic(
                                "Kali's Flower Diadem",
                                "When taking damage that would defeat you, gain full signature charge and briefly become immune to defeat!",
                                "Defense",
                                22,
                                250,
                                3
                        ),
                        Relic(
                                "Large Red Button",
                                "Regain full health every time an exit portal is used! Max health is reduced and all healing has no effect.",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Leemo's Leaf",
                                "Evade all attacks while dashing!",
                                "Defense",
                                24,
                                200,
                                4
                        ),
                        Relic(
                                "Lei's Drum",
                                "Release a burst of lightning when taking damage!",
                                "Defense",
                                15,
                                175,
                                2
                        ),
                        Relic(
                                "Limited Edition Robe",
                                "Reduces damage taken to a maximum of 10% of max health!",
                                "Defense",
                                23,
                                175,
                                4
                        ),
                        Relic(
                                "Lotus Froststone",
                                "Summons an aura that freezes nearby enemies.",
                                "Offense",
                                27,
                                175,
                                5
                        ),
                        Relic(
                                "Memory Chainmail",
                                "Memorizes the last three attacks taken and reduces damage from attacks of the same type!",
                                "Defense",
                                23,
                                175,
                                3
                        ),
                        Relic(
                                "Merchant's Cart",
                                "Increase damage based on number of relics in inventory.",
                                "Offense",
                                22,
                                150,
                                4
                        ),
                        Relic(
                                "Mercury's Sandals",
                                "Increases movement speed!",
                                "Defense",
                                10,
                                125,
                                1
                        ),
                        Relic(
                                "Messy Prescription",
                                "Increases healing received from all sources!",
                                "Doctor",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Mirror Shield",
                                "Summons a shield that reflects one projectile! Shield breaks after reflect and respawns on a timer.",
                                "Defense",
                                14,
                                150,
                                2
                        ),
                        Relic(
                                "Museum Ticket",
                                "A ticket to Lanova Museum's newly opened Chaos Trials exhibit!",
                                "Misc",
                                0,
                                0,
                                1
                        ),
                        Relic(
                                "Mystic Monopole",
                                "Increases knockback when striking foes!",
                                "Misc",
                                11,
                                175,
                                1
                        ),
                        Relic(
                                "Nerite Shell",
                                "Adds a chance to slow foes!",
                                "Offense",
                                10,
                                125,
                                1
                        ),
                        Relic(
                                "Neve's Citrine",
                                "Holding seven lightning arcana increases critical chance! All arcana shocks foes!",
                                "Offense",
                                30,
                                0,
                                5
                        ),
                        Relic(
                                "Neve's Emerald",
                                "Holding seven earth arcana increase health and armor and lowers stun and knockback!",
                                "Defense",
                                30,
                                0,
                                5
                        ),
                        Relic(
                                "Neve's Quartz",
                                "Holding seven air arcana increases movement speed and evasion. All arcana slows foes!",
                                "Defense",
                                30,
                                0,
                                5
                        ),
                        Relic(
                                "Neve's Ruby",
                                "Holding seven fire arcana increases damage! All arcana burns foes!",
                                "Offense",
                                30,
                                250,
                                5
                        ),
                        Relic(
                                "Neve's Sapphire",
                                "Holding seven water arcana reduces all cooldowns and increases healing!",
                                "Misc",
                                30,
                                0,
                                5
                        ),
                        Relic(
                                "Nocturnal Sundial",
                                "Adds a chance that an arcana will not go on cooldown after use!",
                                "Misc",
                                23,
                                175,
                                4
                        ),
                        Relic(
                                "Nog's Heavenly Boots",
                                "Adds a significant chance to evade attacks but take double damage!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Noxious Dappercap",
                                "Adds a chance to poison foes!",
                                "Offense",
                                10,
                                150,
                                1
                        ),
                        Relic(
                                "Ominous Loan Note",
                                "Immediately receive a large sum of gold! All gold gained, with interest, will go toward paying off this debt.",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Overpriced Insurance",
                                "Expend all gold at the start of each stage and heal for a fraction of the expended amount.",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Paronomasicon",
                                "Your attacks on council members are more pun-ishing! Enemy attacks pack an extra pun-ch.",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Pathfinder's Knapsack",
                                "On clearing the floor, receive a heal based on the map reveal percentage.",
                                "Defense",
                                28,
                                200,
                                5
                        ),
                        Relic(
                                "Pathfinder's Map",
                                "Reveals the layout and main points of interest of the Chaos Trials!",
                                "Misc",
                                18,
                                150,
                                3
                        ),
                        Relic(
                                "Pazu's Favorite Hat",
                                "Revive with low health when defeated! Hat can also create health orbs when dropped. This relic is destroyed on use.",
                                "Defense",
                                22,
                                250,
                                3
                        ),
                        Relic(
                                "Perfect Time Crystal",
                                "Signature damage, charge rate, and decay rate increased!",
                                "Offense",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Permafrost Cube",
                                "Adds a chance to freeze foes!",
                                "Offense",
                                10,
                                125,
                                1
                        ),
                        Relic(
                                "Phoenix Talon",
                                "Adds a chance to burn foes!",
                                "Offense",
                                10,
                                150,
                                1
                        ),
                        Relic(
                                "Phyyrnx's Hourglass",
                                "Designed by Artisan PerdyDarnSavvy, this relic activates when taking damage at low health and briefly removes the cooldown on all arcana!",
                                "Misc",
                                15,
                                175,
                                2
                        ),
                        Relic(
                                "Plaguing Sprite Dria",
                                "Summons a sprite that poisons enemies! Only one sprite can be active at any time.",
                                "Misc",
                                14,
                                150,
                                2
                        ),
                        Relic(
                                "Poem of Fiery Rime",
                                "Increases fire and ice damage! Adds a chance to freeze and burn foes to fire and ice arcana, respectively.",
                                "Offense",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Pop-up Primer",
                                "Increases the number of summoned agents, but lowers their health!",
                                "Misc",
                                28,
                                200,
                                5
                        ),
                        Relic(
                                "Puffy Parka",
                                "Evade all critical hits!",
                                "Defense",
                                24,
                                100,
                                5
                        ),
                        Relic(
                                "Raffle Ticket",
                                "Adds a chance that an item purchased in the Chaos Trials will be free of charge.",
                                "Misc",
                                28,
                                200,
                                5
                        ),
                        Relic(
                                "Raspberry Cookie Box",
                                "Health orbs drop more frequently!",
                                "Defense",
                                11,
                                175,
                                1
                        ),
                        Relic(
                                "Reactive Vinemail",
                                "Releases a grove of rooting vines when taking damage!",
                                "Defense",
                                19,
                                175,
                                3
                        ),
                        Relic(
                                "Regenerative Inkwell",
                                "Using a fully charged signature arcana heals you instead of producing a signature spell!",
                                "Defense",
                                28,
                                200,
                                5
                        ),
                        Relic(
                                "Reinforced Bracers",
                                "Your projectiles destroy any other projectiles they strike!",
                                "Misc",
                                28,
                                200,
                                5
                        ),
                        Relic(
                                "Relic Rewards Card",
                                "All gold costs for relics are discounted!",
                                "Misc",
                                12,
                                100,
                                2
                        ),
                        Relic(
                                "Renewing Potion Vial",
                                "Regain a small amount of health when teleporting to the next area!",
                                "Doctor",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Resolute Svalinn",
                                "Increases resistance to fire based attacks!",
                                "Defense",
                                15,
                                175,
                                2
                        ),
                        Relic(
                                "Ring of Recycling",
                                "Gain a shield every time a charged signature is not activated and times out.",
                                "Defense",
                                24,
                                200,
                                4
                        ),
                        Relic(
                                "Ring of Reserves",
                                "Adds more uses to all multi-use arcana!",
                                "Misc",
                                11,
                                175,
                                1
                        ),
                        Relic(
                                "Roxel's Pendulum",
                                "Reduces all cooldowns!",
                                "Misc",
                                10,
                                125,
                                1
                        ),
                        Relic(
                                "Royal Flush",
                                "Equipping arcana of the same element increases damage!",
                                "Offense",
                                18,
                                150,
                                3
                        ),
                        Relic(
                                "Rudra's Pinwheel",
                                "Increases air damage!",
                                "Offense",
                                14,
                                150,
                                2
                        ),
                        Relic(
                                "Sano's Headband",
                                "You can no longer be interrupted while casting an arcana!",
                                "Misc",
                                21,
                                125,
                                4
                        ),
                        Relic(
                                "Scissors of Vitality",
                                "Sacrifice all standard arcana in your hand to revive from defeat!",
                                "Defense",
                                26,
                                250,
                                4
                        ),
                        Relic(
                                "Secret Wild Card",
                                "Your signature will take longer to charge, but can now be used twice on a single charge!",
                                "Offense",
                                23,
                                175,
                                4
                        ),
                        Relic(
                                "Sharpened Stylus",
                                "Increases damage but signature can no longer be charged!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Shiva's Water Bottle",
                                "Increases water damage!",
                                "Offense",
                                14,
                                150,
                                2
                        ),
                        Relic(
                                "Sidewinder's Badge",
                                "All melee basic arcana have a secondary hit that deals reduced damage!",
                                "Offense",
                                27,
                                175,
                                5
                        ),
                        Relic(
                                "Silver Spinning Top",
                                "Increases the activation speed of arcana but slows movement speed!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Singing Bowl",
                                "Not using arcana for a short period of time causes your next spell to be a critical hit!",
                                "Offense",
                                19,
                                175,
                                3
                        ),
                        Relic(
                                "Sinister Ledger",
                                "Designed by Artisan Steven Wu, this relic increases all damage for every enemy defeated, but loses effectiveness when taking damage.",
                                "Offense",
                                15,
                                175,
                                2
                        ),
                        Relic(
                                "Sleepy Thunderstone",
                                "Summons an aura that calls down lightning on nearby enemies.",
                                "Offense",
                                27,
                                175,
                                5
                        ),
                        Relic(
                                "Sparking Sprite Etra",
                                "Summons a sprite that shocks enemies! Only one sprite can be active at any time.",
                                "Misc",
                                23,
                                175,
                                4
                        ),
                        Relic(
                                "Special Snowflake",
                                "Defeated enemies have a chance to explode and freeze nearby enemies!",
                                "Offense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Spell Thief's Socks",
                                "Increase movement speed everytime an arcana is used! Speed is increased for each arcana on cooldown!",
                                "Defense",
                                27,
                                175,
                                5
                        ),
                        Relic(
                                "Spice Rack",
                                "Using an arcana of a different element reduces the cooldown of the previously used arcana.",
                                "Misc",
                                28,
                                200,
                                5
                        ),
                        Relic(
                                "Spiked Emergency Kit",
                                "Regenerate health while at critically low health! Max health is reduced.",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Stanza of Flames",
                                "All fire arcana cause burning but take double damage from water spells! Second half of the Poem of Fiery Rime.",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Stanza of Frost",
                                "Increases ice damage! First half of the Poem of Fiery Rime.",
                                "Offense",
                                18,
                                150,
                                3
                        ),
                        Relic(
                                "Stygian Turtle Shell",
                                "Briefly become invulnerable after taking damage in quick succession!",
                                "Defense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Super Carrot Cake",
                                "Taking damage increases your max health up to a certain limit!",
                                "Defense",
                                14,
                                150,
                                2
                        ),
                        Relic(
                                "Super Sunscreen",
                                "Prevents burn status effect!",
                                "Defense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Supply Crate",
                                "The relic and arcana stores will instantly restock after purchase! Health potions are of short supply and will not be restocked.",
                                "Misc",
                                22,
                                50,
                                5
                        ),
                        Relic(
                                "Surefire Rocket",
                                "Signature charge no longer decays while building up!",
                                "Misc",
                                11,
                                175,
                                1
                        ),
                        Relic(
                                "Takeout Box",
                                "When healed for more than your maximum health, gain a shield equal to half of the excess heal amount.",
                                "Defense",
                                23,
                                175,
                                4
                        ),
                        Relic(
                                "Tapping Gloves",
                                "Following up a basic arcana with another arcana increases its critical hit chance!",
                                "Offense",
                                23,
                                175,
                                4
                        ),
                        Relic(
                                "Tea of Mercy",
                                "All heals increase your max health up to a certain limit!",
                                "Defense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Tears of Midas",
                                "Gain gold when taking damage!",
                                "Misc",
                                25,
                                125,
                                5
                        ),
                        Relic(
                                "Tesla Coil",
                                "Adds a chance to shock foes!",
                                "Offense",
                                10,
                                150,
                                1
                        ),
                        Relic(
                                "Thesis on Defense",
                                "Every point of damage taken adds a page to this thesis. Every 200 pages reduces all damage taken by 1. (Maximum 600 pages)",
                                "Defense",
                                17,
                                125,
                                3
                        ),
                        Relic(
                                "Three Gorges Bulwark",
                                "Increases resistance to water based attacks!",
                                "Defense",
                                15,
                                175,
                                2
                        ),
                        Relic(
                                "Tiny Crocodile Heart",
                                "Reduces max health but increases max health for each enemy defeated!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Tortoise Shield",
                                "Increase armor but decrease damage!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Tozy's Pocket Watch",
                                "Designed by Artisan Curt 'Tozy' Toczydlowski, this relic reduces cooldowns but also reduces signature charge rate!",
                                "Misc",
                                15,
                                175,
                                2
                        ),
                        Relic(
                                "Tracking Suit",
                                "Arcana of matching elements increases resistance to that element! Requires two or more arcana of the same element.",
                                "Defense",
                                26,
                                150,
                                5
                        ),
                        Relic(
                                "Unicorn Tail",
                                "Rainbows!",
                                "Misc",
                                24,
                                100,
                                5
                        ),
                        Relic(
                                "Vampire's Eyeglasses",
                                "Regenerate health every time you land a critical hit!",
                                "Defense",
                                10,
                                150,
                                1
                        ),
                        Relic(
                                "Vampire's Fangs",
                                "Defeating enemies regenerates health but max health is reduced!",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Volatile Gemstone",
                                "Signature charge rate and decay are increased dramatically but charged signature damage is reduced! Shell of the Perfect Time Crystal.",
                                "Cursed",
                                0,
                                0,
                                null
                        ),
                        Relic(
                                "Wallet of Vigor",
                                "Enables you to purchase items with health when you don't have sufficient gold.",
                                "Misc",
                                20,
                                100,
                                4
                        ),
                        Relic(
                                "Wanderer's Mechanism",
                                "Increases the damage of your signature arcana! Core of the Perfect Time Crystal.",
                                "Offense",
                                19,
                                175,
                                3
                        ),
                        Relic(
                                "Whimsical Explodium",
                                "Adds a chance that enemies will explode when defeated!",
                                "Offense",
                                26,
                                125,
                                5
                        ),
                        Relic(
                                "Yuna's Storybook",
                                "Increases the duration of summoned agents!",
                                "Misc",
                                18,
                                150,
                                3
                        )
                )
    }
}