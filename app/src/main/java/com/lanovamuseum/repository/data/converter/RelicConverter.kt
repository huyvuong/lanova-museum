package com.lanovamuseum.repository.data.converter

import com.lanovamuseum.model.Relic as RelicModel
import com.lanovamuseum.repository.data.entity.Relic as RelicEntity

object RelicConverter {
    fun toModel(entity: RelicEntity): RelicModel =
            RelicModel(
                    name = entity.name,
                    description = entity.description,
                    type = entity.type,
                    costInChaosGems = entity.costInChaosGems,
                    costInGold = entity.costInGold,
                    pool = entity.pool)
}