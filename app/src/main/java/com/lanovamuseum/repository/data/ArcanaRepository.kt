package com.lanovamuseum.repository.data

import androidx.lifecycle.LiveData
import com.lanovamuseum.extension.LiveDataExtensions.map
import com.lanovamuseum.model.Arcana
import com.lanovamuseum.repository.data.converter.ArcanaConverter
import com.lanovamuseum.repository.data.dao.ArcanaDao

class ArcanaRepository(private val arcanaDao: ArcanaDao) {
    fun getAll(): LiveData<List<Arcana>> =
            arcanaDao.getAll().map { it.map(ArcanaConverter::toModel) }

    fun search(searchQuery: String): LiveData<List<Arcana>> =
            arcanaDao.search(searchQuery).map { it.map(ArcanaConverter::toModel) }
}