package com.lanovamuseum.repository.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.lanovamuseum.repository.data.entity.Relic

@Dao
interface RelicDao {
    @Query("SELECT * FROM relics;")
    fun getAll(): LiveData<List<Relic>>

    @Query("SELECT * FROM relics WHERE name LIKE '%' || :searchQuery || '%' OR description LIKE '%' || :searchQuery || '%' COLLATE NOCASE;")
    fun search(searchQuery: String): LiveData<List<Relic>>

    @Insert
    fun insertAll(relics: List<Relic>)
}