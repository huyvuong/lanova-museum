package com.lanovamuseum.repository.data

import androidx.lifecycle.LiveData
import com.lanovamuseum.extension.LiveDataExtensions.map
import com.lanovamuseum.model.Relic
import com.lanovamuseum.repository.data.converter.RelicConverter
import com.lanovamuseum.repository.data.dao.RelicDao

class RelicRepository(private val relicDao: RelicDao) {
    fun getAll(): LiveData<List<Relic>> =
            relicDao.getAll().map { it.map(RelicConverter::toModel) }

    fun search(searchQuery: String): LiveData<List<Relic>> =
            relicDao.search(searchQuery).map { it.map(RelicConverter::toModel) }
}