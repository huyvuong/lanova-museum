package com.lanovamuseum.repository.data.converter

import com.lanovamuseum.model.Robe as RobeModel
import com.lanovamuseum.repository.data.entity.Robe as RobeEntity

object RobeConverter {
    fun toModel(entity: RobeEntity): RobeModel =
            RobeModel(
                    name = entity.name,
                    effects = entity.effects)
}