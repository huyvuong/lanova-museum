package com.lanovamuseum.repository.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.lanovamuseum.repository.data.converter.ListTypeConverters
import com.lanovamuseum.repository.data.dao.ArcanaDao
import com.lanovamuseum.repository.data.dao.RelicDao
import com.lanovamuseum.repository.data.dao.RobeDao
import com.lanovamuseum.repository.data.entity.Arcana
import com.lanovamuseum.repository.data.entity.Relic
import com.lanovamuseum.repository.data.entity.Robe
import java.util.concurrent.Executors

@Database(entities = [Relic::class, Arcana::class, Robe::class], version = 1)
@TypeConverters(ListTypeConverters::class)
abstract class LanovaMuseumDatabase : RoomDatabase() {
    abstract fun relicDao(): RelicDao

    abstract fun arcanaDao(): ArcanaDao

    abstract fun robeDao(): RobeDao

    // Kotlin objects aren't as perfect of singletons as using an enum the way Effective Java would
    // suggest, but we're only trying to safeguard against multiple instances because instantiation
    // is expensive; having multiple instances doesn't compromise on program correctness.
    companion object {
        private var instance: LanovaMuseumDatabase? = null

        fun getInstance(context: Context): LanovaMuseumDatabase {
            if (instance == null) {
                synchronized(LanovaMuseumDatabase::class) {
                    instance =
                            Room
                                    .databaseBuilder(
                                            context.applicationContext,
                                            LanovaMuseumDatabase::class.java,
                                            "lanova_museum.db")
                                    .addCallback(
                                            object : Callback() {
                                                override fun onCreate(db: SupportSQLiteDatabase) {
                                                    super.onCreate(db)
                                                    prepopulateRelics()
                                                    prepopulateArcana()
                                                    prepopulateRobes()
                                                }

                                                private fun prepopulateRelics() {
                                                    Executors.newSingleThreadScheduledExecutor()
                                                            .execute {
                                                                getInstance(context)
                                                                        .relicDao()
                                                                        .insertAll(Relic.ALL_RELICS)
                                                            }
                                                }

                                                private fun prepopulateArcana() {
                                                    Executors.newSingleThreadScheduledExecutor()
                                                            .execute {
                                                                getInstance(context)
                                                                        .arcanaDao()
                                                                        .insertAll(
                                                                                Arcana.ALL_ARCANA)
                                                            }
                                                }

                                                private fun prepopulateRobes() {
                                                    Executors.newSingleThreadScheduledExecutor()
                                                            .execute {
                                                                getInstance(context)
                                                                        .robeDao()
                                                                        .insertAll(
                                                                                Robe.ALL_ROBES)
                                                            }
                                                }
                                            })
                                    .build()
                }
            }
            return instance!!
        }
    }
}