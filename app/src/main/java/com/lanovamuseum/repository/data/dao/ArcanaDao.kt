package com.lanovamuseum.repository.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.lanovamuseum.repository.data.entity.Arcana

@Dao
interface ArcanaDao {
    @Query("SELECT * FROM arcana;")
    fun getAll(): LiveData<List<Arcana>>

    @Query("SELECT * FROM arcana WHERE name LIKE '%' || :searchQuery || '%' OR description LIKE '%' || :searchQuery || '%' COLLATE NOCASE;")
    fun search(searchQuery: String): LiveData<List<Arcana>>

    @Insert
    fun insertAll(arcana: List<Arcana>)
}