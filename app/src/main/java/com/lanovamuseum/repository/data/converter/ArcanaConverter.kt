package com.lanovamuseum.repository.data.converter

import com.lanovamuseum.model.Arcana as ArcanaModel
import com.lanovamuseum.repository.data.entity.Arcana as ArcanaEntity

object ArcanaConverter {
    fun toModel(entity: ArcanaEntity): ArcanaModel =
            ArcanaModel(
                    name = entity.name,
                    description = entity.description,
                    element = entity.element,
                    type = entity.type,
                    damage = entity.damage,
                    knockback = entity.knockback,
                    cooldown = entity.cooldown,
                    duration = entity.duration,
                    costInChaosGems = entity.costInChaosGems,
                    costInGold = entity.costInGold,
                    pool = entity.pool)
}