package com.lanovamuseum.repository.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "robes")
data class Robe(
        @PrimaryKey(autoGenerate = true) var id: Long?,
        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "effects") var effects: List<String>
) {
    constructor() : this(null, "", emptyList<String>())

    constructor(name: String, effects: List<String>) : this(null, name, effects)

    companion object {
        val ALL_ROBES =
                listOf(
                        Robe(
                                "Hope",
                                listOf(
                                        "+5% Max Health",
                                        "+8% Run Speed",
                                        "+5% Damage",
                                        "+6% Critical Chance")),
                        Robe(
                                "Patience",
                                listOf(
                                        "+5% Max Health",
                                        "+8% Run Speed",
                                        "+4% Armor",
                                        "+5% Evade Chance")),
                        Robe(
                                "Vigor",
                                listOf(
                                        "+10% Max Health",
                                        "+1 Defense")),
                        Robe(
                                "Pace",
                                listOf(
                                        "+16% Run Speed",
                                        "+5% Evade Chance")),
                        Robe(
                                "Grit",
                                listOf(
                                        "+8% Armor",
                                        "+1 Defense")),
                        Robe(
                                "Shift",
                                listOf(
                                        "+10% Evade Chance",
                                        "+6% Critical Chance")),
                        Robe(
                                "Tempo",
                                listOf(
                                        "–12% Cooldowns",
                                        "+8% Run Speed")),
                        Robe(
                                "Awe",
                                listOf(
                                        "+12% Critical Chance",
                                        "+20% Critical Damage")),
                        Robe(
                                "Rule",
                                listOf(
                                        "+10% Damage",
                                        "+4% Armor")),
                        Robe(
                                "Venture",
                                listOf(
                                        "–40% Max Health",
                                        "+16% Run Speed",
                                        "–12% Cooldowns",
                                        "+10% Damage")),
                        Robe(
                                "Pride",
                                listOf(
                                        "1 HP"))
                )
    }
}