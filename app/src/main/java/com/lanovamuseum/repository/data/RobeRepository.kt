package com.lanovamuseum.repository.data

import androidx.lifecycle.LiveData
import com.lanovamuseum.extension.LiveDataExtensions.map
import com.lanovamuseum.model.Robe
import com.lanovamuseum.repository.data.converter.RobeConverter
import com.lanovamuseum.repository.data.dao.RobeDao

class RobeRepository(private val robeDao: RobeDao) {
    fun getAll(): LiveData<List<Robe>> =
            robeDao.getAll().map { it.map(RobeConverter::toModel) }

    fun search(searchQuery: String): LiveData<List<Robe>> =
            robeDao.search(searchQuery).map { it.map(RobeConverter::toModel) }
}